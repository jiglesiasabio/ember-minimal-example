## ember.js + ember data minimal example
A simple (and minimal) ember.js example featuring a post element using the FIXTURE adapter.


### "Features"
* index view - http://localhost:8888/ember-minimal-example/
* post list view - http://localhost:8888/ember-minimal-example/#/posts/
* single post view - http://localhost:8888/ember-minimal-example/#/posts/1

### To Dos
* linking

(will expand this readme someday)