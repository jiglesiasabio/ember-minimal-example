App = Ember.Application.create();


// adpater definition just for check
App.Store = DS.Store.extend({
  revision: 12,
  adapter: DS.FixtureAdapter.create()
});




App.Router.map(function() {
  this.resource('posts');
  this.resource('post', { path: '/posts/:post_id' });
});


App.PostsRoute = Ember.Route.extend({
  model: function() {
    return App.Post.find();
  }
});

App.PostRoute = Ember.Route.extend({
  model: function(params) {
    return App.Post.find(params.post_id);
  }
});



App.PostsController = Ember.ArrayController.extend();

// ---


App.Post = DS.Model.extend({
    post_id: DS.attr('number'),
    title: DS.attr('string'),
    text: DS.attr('string')
})

App.Post.FIXTURES = [
    {"id": 1, "title": "This is the #1 post", "text":"bla bla bla"},
    {"id": 2, "title": "This is the #2 post", "text":"ble ble ble"},
    {"id": 3, "title": "This is the #3 post", "text":"bli bli bli"}
];
